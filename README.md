### What is this? ###

* This application allows to rename directories that supposed to contain songs from music album of an artist. It's designed for those who like to see music directory structure in standartized view.

### What can it do? ###

* Only few patterns supported by now. Output format: "[year] NameOfDir". Any other information (type, codec, bitrate etc) ignores.

### What can I do? ###

* Browse code by opening solution (*.sln) file with Visual Studio 2012 (or higher).