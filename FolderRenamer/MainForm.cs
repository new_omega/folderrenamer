﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FolderRenamer
{
    public partial class MainForm : Form
    {
        string rootFolder;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            gridViewOriginalNames.Rows.Clear();
            string[] pathList = (string[])e.Data.GetData(DataFormats.FileDrop);
            if (pathList.Length == 1)
            {
                DirectoryInfo root = new DirectoryInfo(pathList[0]);
                DirectoryInfo[] children = root.GetDirectories();
                pathList = new string[children.Length];
                for (int i = 0; i < children.Length; i++)
                    pathList[i] = children[i].FullName;
            }
            for (int i = 0; i < pathList.Length; i++)
            {
                DirectoryInfo di = new DirectoryInfo(pathList[i]);
                if (di.Exists)
                    gridViewOriginalNames.Rows.Add(di.Name);
                rootFolder = di.Parent.FullName;
            }

            textBoxRootFolder.Text = rootFolder;
        }

        private List<string> ConvertNames(List<string> names)
        {
            List<string> result = new List<string>();
            foreach (string name in names)
            {
                result.Add(ConvertName(name));
            }

            return result;
        }

        private string ConvertName(string name)
        {
            string year = "",
                title = "",
                edition = "",
                bitrate = "";
            string[] patterns = { @"(\d{4})(\s-\s)?(.+)(\(\d{2,4}\s?\w+\))", @"(\d{4})(\s-\s)?(.+)(\(\d{2,4}\s?\w+\))?" };
            foreach (string pattern in patterns)
            {
                Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                var result = rgx.Matches(name);
                if (result.Count == 0) continue;
                foreach (Match m in result)
                {
                    for (int i = 1; i < m.Groups.Count; i++)
                    {
                        year = m.Groups[1].Value;
                        title = m.Groups[3].Value;
                    }
                }
            }

            if (year == "") return name;
            else return string.Format("[{0}] {1}", year, title.Trim());
        }

        private void buttonRename_Click(object sender, EventArgs e)
        {
            gridViewRenames.Rows.Clear();
            List<string> oldNames = new List<string>();
            foreach (DataGridViewRow row in gridViewOriginalNames.Rows)
                oldNames.Add(row.Cells[0].Value.ToString());
            List<string> newNames = ConvertNames(oldNames);
            foreach (string newName in newNames)
                gridViewRenames.Rows.Add(newName);

            buttonOk.Enabled = true;
        }

        private void gridViewOriginalNames_DragEnter(object sender, DragEventArgs e)
        {
            MainForm_DragEnter(sender, e);
        }

        private void gridViewOriginalNames_DragDrop(object sender, DragEventArgs e)
        {
            MainForm_DragDrop(sender, e);
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridViewOriginalNames.RowCount; i++)
            {
                Directory.Move(
                    rootFolder + "\\" + gridViewOriginalNames[0, i].Value.ToString(),
                    rootFolder + "\\" + gridViewRenames[0, i].Value.ToString());
            }

            gridViewOriginalNames.Rows.Clear();
            gridViewRenames.Rows.Clear();
            buttonOk.Enabled = false;
        }
    }
}
